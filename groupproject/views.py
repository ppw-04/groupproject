from django.shortcuts import render, redirect
import signup.urls
from signup import views


def homepage(request):
    context = {
        'judul' : 'homepage'

    }
    return render(request, 'homepage.html', context)