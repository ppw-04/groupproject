from django.shortcuts import render, redirect

# Create your views here.
from .forms import LoginForm
from .models import Login

def login(request):
	login_form = LoginForm(request.POST or None)

	if request.method == 'POST':
		if login_form.is_valid():
			login_form.save()

			return redirect('lihat_spaces')

	context = {
		'page_title' : 'Please log in to your account first to book your space.',
		'logins' : login_form,
	}

	return render (request, 'login.html', context)