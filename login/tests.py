from django.test import TestCase
from django.test import SimpleTestCase
from django.urls import reverse,resolve
from login.models import Login
from login.forms import LoginForm
from login.views import login
import json

# Create your tests here.

class TestUrls(TestCase):
    def test_sign_url_resolve(self):
        url = reverse('login')
        self.assertEquals(resolve(url).func,login) 

class TestModles(TestCase):
    def test_setup(self):
        self.Login1 = LoginForm.objects.create(
        email = "kakpewe@gmail.com",
        password = "spacenowproject",
    )
        self.modeltest(self.LoginForm1)
    def modeltest(self,Signupmodel):
        self.assertEquals(self.LoginForm1.email, "kakpewe@gmail.com")
        self.assertEquals(self.LoginForm1.password, "spacenowproject")

class Testforms(SimpleTestCase):
    def test_form(self):
        form = LoginForm(data={
            'email' : "kakpewe@gmail.com",
            'password': 'spacenowproject'
        })
        self.assertTrue(form.is_valid())

class TestViews(TestCase):
    def setup(self):
        self.client = Client()

    def test_login(self):
        response = self.client.get(reverse('login'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'login.html')

        