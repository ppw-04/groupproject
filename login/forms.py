from django import forms

from .models import Login

class LoginForm(forms.ModelForm):
    class Meta:
        model = Login
        fields = [
            'email',
            'password',
        ]

        widgets ={
            'email': forms.TextInput(
                attrs = {
                    'class':'form_control',
                }
            ),
            'password': forms.TextInput(
                attrs = {
                    'class':'form_control',
                }
            )
        }