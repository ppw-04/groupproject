from django.shortcuts import render
from .forms import ContactForms
from .models import ContactModels
from django.http import HttpResponseRedirect

# Create your views here.
def contact(request):
    createMessage = ContactForms(request.POST or None)
    if (request.method == 'POST' and createMessage.is_valid()) :
        simpen = ContactModels(
            name = createMessage.data['name'],
            email = createMessage.data['email'],
            message = createMessage.data['message'],
        )
        simpen.save()
        return HttpResponseRedirect('/Contact/')
    create_Message ={
        'messageForm' : createMessage,
    }
    return render(request, 'contactpage/contact.html', create_Message)
