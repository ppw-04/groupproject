from django import forms

class ContactForms(forms.Form):
    name = forms.CharField(widget = forms.Textarea(attrs={'cols': 50, 'rows': 1}))
    email = forms.EmailField(widget = forms.Textarea(attrs={'cols': 50, 'rows': 1}))
    message = forms.CharField(widget = forms.Textarea(attrs={'cols': 50, 'rows': 7}))