Anggota Kelompok :  
Naura Khansa Priono - 1806191036 
Muhammad Fiqri Adrian 1806190973
Kyosena Haridza  1806186894
Yunie Debora - 1806147224

[![pipeline status](https://gitlab.com/ppw-04/groupproject/badges/master/pipeline.svg)](https://gitlab.com/ppw-04/groupproject/commits/master)

Link Herokuapp:  https://space-now.herokuapp.com/



Aplikasi yang diajukan serta kebermanfaatannya : 

Aplikasi yang kami ajukan adalah aplikasi penyewaan ruangan di seluruh Indonesia. Aplikasi ini berbentuk website dan dapat digunakan untuk menyewa ruangan untuk keperluan meeting, event, serta creative studio. Selain itu aplikasi kami memuat fitur- fitur yang dapat memudahkan dan mengefektifkan penyewa dalam menyewa ruangan, karena aplikasi yang dibuat menampilkan ruangan yang tersedia berdasarkan lokasi, menampilkan gambar ruangan , harga, kapasitas dan pengguna juga dapat langsung memesan ruangan melalui website kami. Ditambah lagi, pengguna dapat melakukan estimasi harga untuk ruang yang akan disewa. 


Daftar fitur yang akan ditampilkan:
Menu bar 
Subscribe button and form input 
Sign Up button and form input 
Log In Subscribe button and form input
Feedback button and form input 
Location form input 
Space type form input
Find space button 
Rent space button 
Social media buttons 
Forms untuk mengestimasi harga 







Page : 
Homepage : Halaman ini berupa landing page dari website yang kami buat ,disini terdapat fitur subscribe button dan form 

Account page : Halaman ini berisi log in page dan sign up page bagi pengguna untuk masuk ke akun mereka sebelum bisa melakukan penyewaan ruangan. Halaman pertama yang akan ditampilkan adalah Log in page, jika user belum memiliki akun maka akan di direct ke sign up page.

Spaces page:  Halaman ini menampilkan ruangan yang tersedia untuk disewa, pengguna bisa mencari ruangan berdasarkan lokasi serta tipe ruangan yang diinginkan. Selain itu ruangan yang ditampilkan di website berisi informasi seperti detil lokasi , kapasitas ruangan , serta harga penyewaan per jam nya. Di halaman ini pengunjung juga akan di direct ke page lain yang langsung mengestimasi biaya penyewaan ruangan berdasarkan total jam peminjaman. 

Contact Us page :
Halaman ini memuat link sosial media yang bisa dihubungi jika ingin meraih customer servicenya. Selain itu page ini juga memuat kolom feedback yang bisa diisi oleh pengguna dan langsung ditampilkan di page yang sama. 

