from django import forms 
class Signupform(forms.Form):
    name = forms.CharField(
        label = "Nama",
        max_length = 40
    )
    age = forms.IntegerField(
        label = "Umur"
    )
    phonenumber = forms.IntegerField(
        label = "Nomor Telefon"
    )
    email = forms.EmailField(
        label = "Email",
        max_length=30
    )
    password = forms.CharField(
        label = "Password",
        max_length=30
    )