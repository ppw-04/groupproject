from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Signupform
from .models import Signupmodels
from django.views.generic import CreateView
# from groupproject import views
# from spacespage import views
# from contactpage import views
from login import views
# Create your views here.
def signup(request):
    form = Signupform()
    if request.method == 'POST':
        print("test")
        data = Signupmodels(
            name =request.POST['name'],
            age = request.POST['age'],
            email = request.POST['email'],
            phonenumber = request.POST['phonenumber'],
            password = request.POST['password'],
            )
        data.save()
        # ses = Signupmodels.objects.all()
        # form = Signupform(name = request.POST.get('name'))

        print('test,valid')
        return HttpResponseRedirect('/thanks/')
    else:
        form = Signupform()
    return render(request,"signup.html",{'form' : form})


def thanks(request):
    return render(request,"thanks.html")
