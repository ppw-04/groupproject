from django.test import TestCase, Client
from django.urls import reverse,resolve
from signup.views import signup,thanks
from signup.models import Signupmodels
import json

class TestViews(TestCase):
    def setup(self):
        self.client = Client()

    def test_Signup(self):
        response = self.client.get(reverse('signup'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'signup.html')

    def test_Thanks(self):
        response = self.client.get(reverse('thanks'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'thanks.html')
        