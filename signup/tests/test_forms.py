from django.test import SimpleTestCase
from signup.forms import Signupform

class Testforms(SimpleTestCase):
    def test_form(self):
        form = Signupform(data={
            'name' : 'kyo',
            'age' : 18,
            'phonenumber' : 1000,
            'email' : "obama@gmail.com",
            'password': 'Moe Lester'
        })
        self.assertTrue(form.is_valid())