from django.test import TestCase
from signup.models import Signupmodels
from signup.views import signup,thanks

class TestModles(TestCase):
    def test_setup(self):
        self.Signupmodel1 = Signupmodels.objects.create(
        name = "Tester",
        age = int("18"),
        phonenumber = int("081380805130"),
        email = "TesterMcTestee@gmail.com",
        password = "testmcgee"
    )
        self.modeltest(self.Signupmodel1)
    def modeltest(self,Signupmodel):
        self.assertEquals(self.Signupmodel1.name,"Tester")
        self.assertEquals(self.Signupmodel1.age, 18)
        self.assertEquals(self.Signupmodel1.phonenumber,int("081380805130"))
        self.assertEquals(self.Signupmodel1.email, "TesterMcTestee@gmail.com")
        self.assertEquals(self.Signupmodel1.password, "testmcgee")