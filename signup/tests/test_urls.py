from django.test import SimpleTestCase
from django.urls import reverse,resolve
from signup.views import signup,thanks

class TestUrls(SimpleTestCase):
    def test_sign_url_resolve(self):
        url = reverse('signup')
        self.assertEquals(resolve(url).func,signup) 

    def test_thanks_url_resolve(self):
        url = reverse('thanks')
        self.assertEquals(resolve(url).func,thanks)