from django.db import models
from django.core.validators import MaxValueValidator
class Signupmodels(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(
        max_length = 40,
        default =" "
    )
    age = models.IntegerField(
        default = 18,
        validators = [MaxValueValidator(999999999999)]
    )
    phonenumber = models.IntegerField(

        default = 0
    )
    email = models.EmailField(
        max_length = 30
    )
    password = models.CharField(
        default = "password",
        max_length = 30
    )
# Create your models here.
