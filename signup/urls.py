from django.urls import path, include
from signup import views as views

from spacespage import views as spaceview
from groupproject import views as splash
from contactpage import views as contactview
from login import views as loginview

urlpatterns = [
    path('Signup/',views.signup,name = "signup"),
    path('thanks/',views.thanks,name = 'thanks'),
    path('login/',loginview.login,name = "login")

    
] 