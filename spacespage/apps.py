from django.apps import AppConfig


class SpacespageConfig(AppConfig):
    name = 'spacespage'
