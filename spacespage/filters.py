import django_filters 

from .models import Spacesmodels

class SpacesFilter(django_filters.FilterSet):
    nama = django_filters.CharFilter(lookup_expr='icontains')
    kota = django_filters.CharFilter(lookup_expr='icontains')
    tipe = django_filters.CharFilter(lookup_expr='icontains')

    class Meta: 
        model = SpacesModels