from django.shortcuts import render, redirect,get_object_or_404
from django.http import *
from django.http import HttpResponseRedirect
from .forms import Spaces
from .models import Spacesmodels


from django.contrib import messages

def listfilter(request):
	ruangan = Spacesmodels.object.all()
	filter = SpacesFilter(request.GET, queryset = ruangan)
	return render(request, 'spacespage/spaces2.html', {'filter' : filter})

def lihatspaces(request):
    all= Spacesmodels.objects.all()

    arg= {
        "previous": all
    }
    return render(request, "spaces2.html",arg)

def addspaces(request):
    isianform= Spaces()
    arg={
        "form" : isianform
    }
    return render(request, 'addspaces.html',arg)

def spaces_tambah(request):
    # getfoto = request.FILES["foto"]
    getnama= request.POST["nama"]
    getkota = request.POST["kota"]
    gettipe= request.POST["tipe"]
    getharga = request.POST["harga"]

    temp = Spacesmodels(
        # foto = getfoto,
        nama = getnama, 
        kota = getkota, 
        tipe = gettipe, 
        harga = getharga, 
    )

    temp.save()

    all= Spacesmodels.objects.all()

    arg= {
        "previous": all
    }
    return render(request, "spaces2.html",arg)
    
def rent(request,id):
    rentee = get_object_or_404(Spacesmodels, pk = id)
    form = Spaces()
    arg = {
        "rentee":rentee,
        "form":form
    }
    if request.method == "POST":
        hours = request.POST['jam']
        rate = rentee.harga
        return pricing(request,rate,hours)
    else:
        return render(request,'rent.html',arg)

def pricing(request,rate,hours):
    price = int(rate) * int(hours)
    arguement = {
        "harga" : price
    }
    return render(request,"pricing.html",arguement)



