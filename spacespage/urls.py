from django.urls import path, include
from . import views

from .views import lihatspaces, addspaces, spaces_tambah,rent,pricing

#buat nyambungin sama app lain (buat di navbar)
from login import views


urlpatterns = [
    #path('spaces/', spaces ,name="spaces"),
    path('addspaces/', addspaces, name= "addspaces"),
    path('spaces_tambah/', spaces_tambah) ,
    path('lihat_spaces/', lihatspaces, name ="lihat_spaces"),
    path('rent/<int:id>',rent,name = "rent"),
    path('login/', views.login),
    path('process/',pricing)
    
  
    

    #url(r'^search/$’, views.listfilter, name='searcher'),

    
]